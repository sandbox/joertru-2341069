<?php 
/**
* Constructor del formulario para configurar el módulo.
*/
function field_content_type_admin_settings() {

  $content_types_list = node_type_get_types();
  foreach ($content_types_list as $key => $type) {
  	$list[$key] = $type->name;
  }
  $form['field_content_type_node_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Select content types for Field Content type'),
    '#options' => $list,
    '#default_value'=> variable_get('field_content_type_node_types', array('page')),
    '#description' => t('The selected content types include field content.'),
  );
 return system_settings_form($form);
}
